import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../context';

export default class Product extends Component {
  render() {
    const { id, title, img, price, inCart } = this.props.product;
    return (
      <ProductWrapper className="col-9 mx-auto col-md-6 col-lg-3 my-3">
        <div className="card">
          <div className="img-container p-5" onClick={() => console.log(1, "test")}>
            <Link to="/details">
              <img src={img} alt="Product" className="card-img-top" />
            </Link>
            <button className="cart-btn" disabled={inCart} onClick={() => {console.log(2, "test")}}>
              { inCart ? (<p className="text-capitalize mb-0" disabled>in cart</p>) : <i className="fas fa-cart-plus" /> }
            </button>
          </div>
          <div className="card-footer d-flex justify-content-between">
            <p className="align-self-center mb-0">{ title }</p>
            <h5 className="text-blue font-italic-mb-0">
              <span className="mr-1">$</span>
              { price }
            </h5>
          </div>
        </div>
      </ProductWrapper>
    );
  }
}

const ProductWrapper = styled.div`
  .card {
    border-color: transparent;
    transition: all .2s linear;
  }

  .card-footer {
    background-color: transparent;
    border-top: transparent;
    transition: all .2s linear;
  }

  .img-container {
    position: relative;
    overflow: hidden;

    &:hover .cart-btn {
      transform: translate(0, 0);
    }
  }

  .card-img-top {
    transition: all .2s linear;
  }

  .cart-btn {
    position: absolute;
    bottom: 0;
    right: 0;
    padding: .2rem .4rem;
    border: none;
    background-color: var(--lightBlue);
    color: var(--mainWhite);
    font-size: 1.4rem;
    border-top-left-radius: .5rem;
    transform: translate(100%, 100%);
    transition: all .2s linear;

    &:hover {
      color: var(--mainBlue);
      cursor: pointer;
    }
  }

  &:hover {
    .card {
      border: .04rem solid rgba(0,0,0,.2);
      box-shadow: 2px 2px 5px 0px rgba(0,0,0,.2);
    }
    .card-footer {
      background-color: rgba(247,247,247);
    }
    .card-img-top {
      transform: scale(1.2);
    }
  }
`;